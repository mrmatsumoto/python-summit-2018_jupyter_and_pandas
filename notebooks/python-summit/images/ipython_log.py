# IPython log file

get_ipython().run_cell_magic('bash', '', 'cat my-shell-script.sh')
Einzelne Bash Befehle in einer Python-Zelle ausführen:
# run in bash
get_ipython().system('echo ""')

# run script in bash and save return value in a python variable
s = get_ipython().getoutput('./my-shell-script.sh')

# pure python
print(s)
get_ipython().run_line_magic('whos', '')
get_ipython().run_line_magic('whos', '')
get_ipython().system('export "Hello"')
get_ipython().system('export my_env_var="Hello"')

get_ipython().system('echxo ')
get_ipython().system('export my_env_var="Hello"')

get_ipython().system('echo $my_env_var')
get_ipython().system('export my_env_var="Hello"')

get_ipython().system('export my_env_var="Hello"; echo $my_env_var')
get_ipython().system('export my_env_var="Hello"')

get_ipython().system('echo $my_env_var')
get_ipython().run_line_magic('dirs', '')
Aktuellen Stack anzeigen:
get_ipython().run_line_magic('pushd', 'images')
get_ipython().run_line_magic('pushd', 'images')
get_ipython().run_line_magic('dirs', '')
&popd
get_ipython().run_line_magic('popd', '')
get_ipython().run_line_magic('dirs', '')
get_ipython().run_line_magic('pushd', 'images')
get_ipython().run_line_magic('pushd', 'images')
get_ipython().run_line_magic('dirs', '')
get_ipython().run_line_magic('pushd', 'images')
get_ipython().run_line_magic('dirs', '')
get_ipython().run_line_magic('pwd', '')
get_ipython().system('ls -la')
get_ipython().run_line_magic('popd', '')

print("\n\n%dirs\n=====\n")
d = get_ipython().run_line_magic('dirs', '')
print(d)
#!ls -la
get_ipython().run_line_magic('popd', '')

print("\n\n%dirs\n=====\n")
d = get_ipython().run_line_magic('dirs', '')
print(d)
#!ls -la
get_ipython().run_line_magic('popd', '')

print("\n\n%dirs\n=====\n")
d = get_ipython().run_line_magic('dirs', '')
print(d)
#!ls -la
get_ipython().run_line_magic('pushd', 'images')
get_ipython().run_line_magic('pushd', 'images')
get_ipython().run_line_magic('pushd', 'images')
get_ipython().run_line_magic('pushd', 'images')
get_ipython().run_line_magic('pushd', 'images')
get_ipython().run_line_magic('dirs', '')
get_ipython().run_line_magic('pwd', '')
get_ipython().system('ls -la')
get_ipython().run_line_magic('popd', '')

print("\n\n%dirs\n=====\n")
d = get_ipython().run_line_magic('dirs', '')
print(d)
#!ls -la
get_ipython().run_line_magic('popd', '')

print("\n\n%dirs\n=====\n")
d = get_ipython().run_line_magic('dirs', '')
print(d)
#!ls -la
get_ipython().run_line_magic('popd', '')

print("\n\n%dirs\n=====\n")
d = get_ipython().run_line_magic('dirs', '')
print(d)
#!ls -la
get_ipython().run_line_magic('popd', '')

print("\n\n%dirs\n=====\n")
d = get_ipython().run_line_magic('dirs', '')
print(d)
#!ls -la
get_ipython().run_line_magic('popd', '')

print("\n\n%dirs\n=====\n")
d = get_ipython().run_line_magic('dirs', '')
print(d)
#!ls -la
get_ipython().run_line_magic('pwd', '')
get_ipython().run_line_magic('popd', '')

print("\n\n%dirs\n=====\n")
d = get_ipython().run_line_magic('dirs', '')
print(d)
#!ls -la
get_ipython().run_line_magic('pwd', '')
get_ipython().run_line_magic('popd', '')

print("\n\n%dirs\n=====\n")
d = get_ipython().run_line_magic('dirs', '')
print(d)
#!ls -la
current_directory = get_ipython().run_line_magic('pwd', '')
print("\n\n Current Directory: " % current_directory)
get_ipython().run_line_magic('pushd', 'images')
get_ipython().run_line_magic('pushd', 'images')
get_ipython().run_line_magic('popd', '')

print("\n\n%dirs\n=====\n")
d = get_ipython().run_line_magic('dirs', '')
print(d)
#!ls -la
current_directory = get_ipython().run_line_magic('pwd', '')
print("\n\n Current Directory: " % current_directory)
get_ipython().run_line_magic('popd', '')

print("\n\n%dirs\n=====\n")
d = get_ipython().run_line_magic('dirs', '')
print(d)
#!ls -la
current_directory = get_ipython().run_line_magic('pwd', '')
print("\n\n Current Directory: %s" % current_directory)
get_ipython().run_line_magic('pushd', 'images')
get_ipython().run_line_magic('pushd', 'images')
get_ipython().run_line_magic('pushd', 'images')
get_ipython().run_line_magic('pushd', 'images')
get_ipython().run_line_magic('popd', '')

print("\n\n%dirs\n=====\n")
d = get_ipython().run_line_magic('dirs', '')
print(d)
#!ls -la
current_directory = get_ipython().run_line_magic('pwd', '')
print("\n\n Current Directory: %s" % current_directory)
get_ipython().run_line_magic('popd', '')

print("\n\n%dirs\n=====\n")
d = get_ipython().run_line_magic('dirs', '')
print(d)
#!ls -la
current_directory = get_ipython().run_line_magic('pwd', '')
print("\n\n Current Directory: %s" % current_directory)
get_ipython().run_line_magic('popd', '')

print("\n\n%dirs\n=====\n")
d = get_ipython().run_line_magic('dirs', '')
print(d)
#!ls -la
current_directory = get_ipython().run_line_magic('pwd', '')
print("\n\n Current Directory: %s" % current_directory)
get_ipython().run_line_magic('popd', '')

print("\n\n%dirs\n=====\n")
d = get_ipython().run_line_magic('dirs', '')
print(d)
#!ls -la
current_directory = get_ipython().run_line_magic('pwd', '')
print("\n\n Current Directory: %s" % current_directory)
get_ipython().run_line_magic('dhist', '')
_dh
History der Verzeichniswechsel anzeigen:
get_ipython().run_line_magic('bookmark', 'base /notebooks/python-summit')
get_ipython().run_line_magic('bookmark', 'img /notebooks/python-summit/images')
get_ipython().run_line_magic('bookmark', '-l')
get_ipython().run_line_magic('cd', 'img')
get_ipython().run_line_magic('logstart', '')
get_ipython().run_line_magic('logstate', '')
get_ipython().run_line_magic('logoff', '')
get_ipython().run_line_magic('logstop', '')
