from IPython.core.display import display, HTML


def display_question(s):
    display(
        HTML(
            '<div style="margin: 1.5em 0 3em 0;">'
            + '<div style="color: orange; font-size: 4em; font-weight: bold; float: left; margin: 0.25em;">?</div>'
            + '<div style="margin-top: 0em; font-size: 1.5em; line-height: 1.2em;">%s</div>' % str(s)
            + '</div>'
        )
    )


def display_exercise(s):
    display(
        HTML(
            '<div style="margin: 2em 0 3em 0;">'
            + '<div style="color: purple; font-size: 1.5em; font-weight: bold; float: left; padding-right: 1em; margin-top: 0.125em; ">Aufgabe:</div>'
            + '<div style="margin-left: 6em; font-size: 1.5em; line-height: 1.2em;">%s</div>' % str(s)
            + '</div>'
        )
    )


def display_problem(s):
    display(
        HTML(
            '<div style="margin: 1.5em 0 3em 0;">'
            + '<div style="color: red; font-size: 4em; font-weight: bold; float: left; margin: 0.25em;">!</div>'
            + '<div style="color: red; margin-top: 0em; font-size: 2em; line-height: 1.5em;">%s</div>' % str(s)
            + '</div>'
        )
    )