import numpy as np
import matplotlib.pyplot as plt


def plot_sample():
    sample_data = np.random.random(30)
    plt.plot(sample_data, color='k', linestyle='dashed', marker='o', markersize=12)


def prepare_polar_sample(graph=None):
    ## source: https://matplotlib.org/gallery/pie_and_polar_charts/polar_scatter.html#sphx-glr-gallery-pie-and-polar-charts-polar-scatter-py
    import numpy as np
    import matplotlib.pyplot as plt
    # Fixing random state for reproducibility
    np.random.seed(19680801)
    # Compute areas and colors
    N = 150
    r = 2 * np.random.rand(N)
    theta = 2 * np.pi * np.random.rand(N)
    area = 200 * r ** 2
    colors = theta

    if not graph:
        fig = plt.figure()
        graph = fig.add_subplot(111, projection='polar')
    c = graph.scatter(theta, r, c=colors, s=area, cmap='hsv', alpha=0.75)


def prepare_luck_graph(graph):
    # source: https://matplotlib.org/gallery/lines_bars_and_markers/scatter_symbol.html#sphx-glr-gallery-lines-bars-and-markers-scatter-symbol-py
    import matplotlib.pyplot as plt
    import numpy as np

    # Fixing random state for reproducibility
    np.random.seed(19680801)

    x = np.arange(0.0, 50.0, 2.0)
    y = x ** 1.3 + np.random.rand(*x.shape) * 30.0
    s = np.random.rand(*x.shape) * 800 + 500

    plt.scatter(x, y, s, c="g", alpha=0.5, marker=r'$\clubsuit$',
                label="Luck")
    plt.xlabel("Leprechauns")
    plt.ylabel("Gold")
    plt.legend(loc='upper left')
    plt.show()


def prepare_mat_graph(graph):
    # source: https://matplotlib.org/1.5.1/examples/pylab_examples/matshow.html
    def samplemat(dims):
        """Make a matrix with all zeros and increasing elements on the diagonal"""
        aa = np.zeros(dims)
        for i in range(min(dims)):
            aa[i, i] = i
        return aa

    graph.matshow(samplemat((15, 15)))