set -e

if [ ! -f /root/.jupyter/jupyter_notebook_config.py ]; then
    cp /opt/jupyter_notebook_config.py /root/.jupyter/jupyter_notebook_config.py
fi

jupyter notebook --allow-root