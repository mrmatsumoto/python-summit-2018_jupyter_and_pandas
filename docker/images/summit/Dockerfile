FROM debian:jessie

ENV LANG=C.UTF-8 LC_ALL=C.UTF-8
ENV PATH /opt/conda/bin:$PATH

RUN apt-get update && \
    apt-get install -y \
    bzip2 \
    git \
    wget \
    gcc \
    && rm -rf /var/lib/apt/lists/*

RUN wget --quiet https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh && \
    chmod +x Miniconda3-latest-Linux-x86_64.sh && \
    ./Miniconda3-latest-Linux-x86_64.sh -b -p /opt/conda && \
    rm Miniconda3-latest-Linux-x86_64.sh

ADD workshop-installation-linux.sh /opt/bin/workshop-installation-linux.sh
RUN chmod +x /opt/bin/workshop-installation-linux.sh
RUN /opt/bin/workshop-installation-linux.sh

ADD entrypoint.sh /opt/bin/entrypoint.sh
RUN chmod +x /opt/bin/entrypoint.sh

ADD jupyter_notebook_config.py /opt/jupyter_notebook_config.py

WORKDIR /notebooks

EXPOSE 8888

ENTRYPOINT /opt/bin/entrypoint.sh
CMD ['jupyter', 'notebook', '--allow-root']